from jax.config import config
config.update("jax_enable_x64", True)

import jax
import jax.numpy as jnp
import jax.random as jr
import numpyro.distributions as dist

__module = jnp
__name = "jax"


def cast(arr, dtype):
    return jax.lax.convert_element_type(arr, new_dtype=dtype)


def indices_where(condition):
    # TODO: generalize
    return jnp.where(condition)[0]


def scatter_nd(indices, updates, shape):
    arr = jnp.zeros(shape, updates.dtype)
    i = jnp.moveaxis(indices, -1, 0)
    return arr.at[i].set(updates)


class RandomNumberGenerator:
    def normal(self, loc=0.0, scale=1.0, size=(), dtype=jnp.float64):
        return dist.Normal(loc, scale). \
                sample(jr.PRNGKey(1), sample_shape=size)

    def truncated_normal(self, loc, scale, low, high, size=(),
                         dtype=jnp.float64):
        return dist.TruncatedNormal(loc, scale, low=low, high=high). \
                sample(jr.PRNGKey(1), sample_shape=size)


# TODO: seeding
rng = RandomNumberGenerator()
