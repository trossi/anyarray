import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

__module = tf
__name = "tensorflow"

concatenate = tf.concat
inf = tf.cast(np.inf, dtype=tf.float64)
isnan = tf.math.is_nan
max = tf.math.reduce_max
take = tf.gather


def indices_where(condition):
    # TODO: generalize
    return tf.where(condition)[:, 0]


class RandomNumberGenerator:
    def normal(self, loc=0.0, scale=1.0, size=(), dtype=tf.float64):
        loc = tf.cast(loc, dtype=dtype)
        return tfd.Normal(loc, scale).sample(size)

    def truncated_normal(self, loc, scale, low, high, size=(),
                         dtype=tf.float64):
        loc = tf.cast(loc, dtype=dtype)
        return tfd.TruncatedNormal(loc, scale, low, high).sample(size)


# TODO: seeding
rng = RandomNumberGenerator()
