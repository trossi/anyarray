import numpy as np

__module = np
__name = "numpy"


def cast(arr, dtype):
    return arr.astype(dtype)


def scatter_nd(indices, updates, shape):
    arr = np.zeros(shape, updates.dtype)
    i = np.moveaxis(indices, -1, 0)
    arr[i] = updates
    return arr


class RandomNumberGenerator:
    def normal(self, loc=0.0, scale=1.0, size=(), dtype=np.float64):
        return np.random.default_rng(123).normal(loc, scale).sample(size)

    def truncated_normal(self, loc, scale, low, high, size=(),
                         dtype=np.float64):
        raise NotImplementedError()
        # from scipy.stats import truncnorm
        # return truncnorm((low - loc) / scale, (high - loc) / scale, loc=loc, scale=scale).rvs(size)


# TODO: seeding
rng = RandomNumberGenerator()
