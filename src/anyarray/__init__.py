__backend = None


def use(backend):
    global __backend
    if backend.lower() == "numpy":
        import anyarray.backends.numpy as __backend
    elif backend.lower() == "tensorflow":
        import anyarray.backends.tensorflow as __backend
    elif backend.lower() == "jax":
        import anyarray.backends.jax as __backend
    else:
        raise KeyError(f"unknown backend: {repr(backend)}")


def __getattr__(key):
    if hasattr(__backend, key):
        return getattr(__backend, key)
    elif hasattr(__backend.__module, key):
        return getattr(__backend.__module, key)
    else:
        raise AttributeError(f"backend {repr(__backend.__name)} "
                             f"has no attribute {repr(key)}")
