# anyarray

## Set up virtual environment

```bash
python3 -m venv venv
source venv/bin/activate
python3 -m pip install -e .
```

## TODO

* Seed random number generators properly
* Generalize `indices_where()`
