from setuptools import setup, find_packages


setup(name="anyarray",
      version="0.0",
      description="anyarray - wrapper for arrays",
      license="MIT",
      packages=find_packages("src"),
      package_dir={"": "src"},
      install_requires=[
          "numpy",
          "tensorflow",
          "jax",
          "numpyro",
          ],
      )
